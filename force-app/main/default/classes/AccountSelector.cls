public class AccountSelector {
	 @AuraEnabled(cacheable=true)
    public static List<Account> getQuote(List<String> quoteId){
        List<Id>  ids = new List<Id>();
          
        return [select id,Opportunity__c, (select id, Name from Contacts) from Account where Id = :quoteId];
    }
}