public class Checkoutreq {    
    @AuraEnabled(cacheable=true)
    public static List<Checked_out_By__c> getRequestsByOpportunity(){
        return [select id, Type__c, Opportunity__c, Opportunity__r.AccountId from Checked_out_By__c];
    }
}