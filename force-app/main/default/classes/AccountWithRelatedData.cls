public class AccountWithRelatedData {
  @AuraEnabled(cacheable=true)
  public static String getRecords(String recordId) {
    if (String.isBlank(recordId)) {
      return '';
    }

    List<Account> accList = [
      SELECT
        Id,
        Name,
        (SELECT Id, Name FROM Contacts),
        (SELECT id, Name FROM Opportunities)
      FROM Account
      WHERE id = :recordId
    ];

    return JSON.serialize(accList);
  }
}