public class BorrowedProject {
	@AuraEnabled(cacheable=true)
    public static List<Borrowed_By__c> getLatestPlanProjectByOpportunity(List<String> opportunityId){
        System.debug('opportunityId : '+opportunityId);
        return [select id,name, Opportunity__c from Borrowed_By__c where Opportunity__c IN :opportunityId order by createddate desc];
    }
}