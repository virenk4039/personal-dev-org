public with sharing class Contactdetails {
	/*  @AuraEnabled(cacheable=true)
    public static List<Contact> getContactList() {
        return [SELECT Id, FirstName, LastName, Title, Phone, Email FROM Contact LIMIT 50];
    }
*/
	 @AuraEnabled(cacheable=true)
    public static  List<SObject> getContactList() {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : SObjectType.contact.FieldSets.contactfileds.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'ID FROM contact LIMIT 50';
        List<SObject> con=Database.query(query);
        return con;
    }
}