global class AccUpdates {
    @future (callout=true)
 public static void Integrationccupdate(id ccid) {
   //Construct HTTP request and response
    HttpRequest req = new HttpRequest();
    req.setHeader('Content-Type','application/json');
    String endpoint = 'https://www.usps.com/business/web-tools-apis/address-information-api.htm';
    req.setMethod('POST');
    req.setEndpoint(endpoint); 
    system.debug('CC ID' +ccid);
    list<Account> Acc=[select id,Name from Account where id=:ccid];
    String JsonString=JSON.serialize(Acc);
    req.setBody(JsonString);
    system.debug(JsonString);
    //Http response method 
    Http http = new Http();
    HTTPResponse res = http.send(req);
    System.debug(res.getBody());

  }
}