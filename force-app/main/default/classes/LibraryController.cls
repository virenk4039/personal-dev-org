public class LibraryController {
      @AuraEnabled
      public static List <Book__c> getItem() {
        return [SELECT Id, name, Type__c, Status__c FROM Book__c ORDER BY createdDate ASC];
      }
    
     @AuraEnabled
    public static List <Book__c> getItemsFiltered(String selectfield,String selectcondition,String Enteredvalue){
       //create list to fetch values based on conditions
        List<Book__c> myitems=new List<Book__c>();
        //Dynamic query

        if(selectcondition=='Equals'){
            string query1='SELECT Id, name, Type__c, Status__c FROM Book__c Where '+ selectfield +'='+'\''+Enteredvalue+'\'';
        myitems =Database.query(query1);
            }
        else if(selectcondition=='Not Equals'){
            string query2='SELECT Id, name, Type__c, Status__c FROM Book__c Where '+ selectfield +'!='+'\''+Enteredvalue+'\'';
           myitems =Database.query(query2);

            }
        else if(selectcondition=='Contains'){
            string query3='SELECT Id, name, Type__c, Status__c FROM Book__c Where '+ selectfield +' like'+'\'%'+Enteredvalue+'%\'' ;
            
            myitems =Database.query(query3);

            }
        
        return myitems;
        
      }

  
}