public class GetchildRecordDetails {
    @AuraEnabled
    public static String getChildRecord(String recordId) {
        System.debug('recordId '+recordId);
        List<Checked_out_By__c> checkoutMaterial = [SELECT Id FROM Checked_out_By__c Where Borrowed_By__c =: recordId];
        System.debug('checkoutMaterial '+checkoutMaterial);
        if(checkoutMaterial.size() > 0) {
            return String.valueof(checkoutMaterial[0].Id);
        }
        return '';
    }
}