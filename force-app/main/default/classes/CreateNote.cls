public class CreateNote {
	 @AuraEnabled
    public static Map<String, Object> saveNotes(String noteTitle, String noteContent, String oppRecord){
        ContentNote cn = new ContentNote();
        cn.title = noteTitle;
        Map<String, Object> outputMap = new Map<String, Object> ();
        string ss = noteContent.stripHtmlTags();
        System.debug('ss====='+ss);
        Blob content = Blob.valueOf(ss.escapeHTML4());
        cn.Content = content;
        try{
            if(cn != null){
                insert cn;
                ContentNote cd = [SELECT id, title, textpreview, content, LastModifiedDate, LastModifiedBy.Name FROM ContentNote where id=:cn.Id];
                ContentVersion cversion = [SELECT  Id, ContentDocumentId,  Title, Description, FileType, FileExtension, TextPreview FROM ContentVersion 
                                           where ContentDocumentId =:cd.Id];
                System.debug('cversion : '+cversion);
                cversion.Description = noteContent;
                update cversion;
                ContentVersion cversion1 = [SELECT  Id, ContentDocumentId,  Title, Description, FileType, FileExtension, TextPreview FROM ContentVersion 
                                           where ContentDocumentId =:cd.Id];
                
                System.debug('cversion 1: '+cversion1);
                
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.ContentDocumentId = cd.id;
                cdl.ShareType = 'V';
                cdl.LinkedEntityId = oppRecord;
                insert cdl;
                outputMap.put('contentNote', cd);
                outputMap.put('contentDocumentLink', cdl);
            }
        }
        catch(DmlException e){
            System.debug('An unexpected error has occurred: ' + e.getDmlMessage(0));
        }
        
        return outputMap;

    }
    
    @AuraEnabled //(cacheable=true)
    public static Map<Id, String> getContents( List<String> noteId){
        Map<Id, String> contentsAsString = new Map<Id, String> ();
	List<Id> contentNoteId = new List<Id>();
        List<ContentNote> cnotes = [SELECT Id, Content FROM ContentNote WHERE Id IN: noteId];
        
        for (ContentNote note : cnotes) {
            contentNoteId.add(note.Id);
           // contentsAsString.put(note.Id, note.Content.toString().unescapeHtml4());
        }
        
        for(ContentVersion contentVersion : [SELECT  Id, ContentDocumentId,  Title, Description, FileType, FileExtension, TextPreview FROM ContentVersion 
                                           where ContentDocumentId IN : contentNoteId]) {
            contentsAsString.put(contentVersion.ContentDocumentId, contentVersion.Description);
        }
        
        return contentsAsString;
    }
}