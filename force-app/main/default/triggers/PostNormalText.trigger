trigger PostNormalText on FeedItem (before insert) {
    
    if(trigger.isBefore) {
        for(FeedItem item : Trigger.New) {            
            if(item.body.contains('@DFW Retail Pursuit Group')) {
                String stringBeforeGroupName = item.body.substringBefore('@DFW Retail Pursuit Group');
                String stringAfterGroupName = item.body.substringAfter('@DFW Retail Pursuit Group');
                System.debug('stringBeforeGroupName'+stringBeforeGroupName);
                System.debug('stringAfterGroupName'+stringAfterGroupName);
               // stringBeforeGroupName = stringBeforeGroupName.removeEnd('@');
                stringBeforeGroupName = stringBeforeGroupName.replace('@','');
                stringAfterGroupName = stringAfterGroupName.replace('(Private)','');
                String finalString = stringBeforeGroupName + '@DFW Retail Pursuit Group' + stringAfterGroupName;
                System.Debug('updatedBody :'+finalString);
                item.Body = finalString;
            }   
        }
    }
}