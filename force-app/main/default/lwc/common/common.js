export const FileImportStatus = {
  NewImport: "New",
  FileUploaded: "File Uploaded",
  FileFailed: "File Failed",
  Completed: "Completed",
  InProgress: "In Progress",
  Approved: "Approved",
  Imported: "Imported",
  TalendFailed: "Talend Failed",
  ImportCompleted: "Imported Completed"
};

export const TabsEnum = {
  BPPProp13Factors: 'BPP Prop 13 Factors'
}

export const Notification = {
  Success: "success",
  Error: "error",
  Warning: "warning"
};

/* Transform map data into Options for combobox*/
export const transformMapDatatoOptions = (mapData, prop = null) => {
  let optionsList = [];
  let lstToHoldMapArray = [];
  for (let key in mapData) {
    if (mapData.hasOwnProperty(key)) {
      if (prop) {
        lstToHoldMapArray.push(mapData[key]);
      }
      optionsList.push({
        value: key,
        label: prop ? mapData[key][prop] : mapData[key]
      });
    }
  }
  return { optionList: optionsList, mapObjArray: lstToHoldMapArray };
};

export const intersetItemsArray = (array1, array2) => {
  let intersectArr = [];
  array1.forEach(a => {
    array2.forEach(b => {
      if (JSON.stringify(a) === JSON.stringify(b)) {
        intersectArr.push(b);
      }
    });
  });
  return intersectArr;
};

export const groupBy = (items, key) =>
  items.reduce(
    (result, item) => ({
      ...result,
      [item && item[key]]: [...(result[item && item[key]] || []), item]
    }),
    {}
  );

export const getBuildEnv = hostNameAddr => {
  let env = "";

  env = hostNameAddr.toLocaleLowerCase().indexOf("dev") > -1 ? "dev" : env;
  env = hostNameAddr.toLocaleLowerCase().indexOf("qa") > -1 ? "qa" : env;
  return env;
};