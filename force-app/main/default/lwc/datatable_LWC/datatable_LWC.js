import { LightningElement, wire, track } from 'lwc';
import getContactList from '@salesforce/apex/Contactdetails.getContactList';
//import { updateRecord } from 'lightning/uiRecordApi';
//import { refreshApex } from '@salesforce/apex';
//import { ShowToastEvent } from 'lightning/platformShowToastEvent';
/*import FIRSTNAME_FIELD from '@salesforce/schema/Contact.FirstName';
import LASTNAME_FIELD from '@salesforce/schema/Contact.LastName';
import Email_FIELD from '@salesforce/schema/Contact.Email';
import Phone_FIELD from '@salesforce/schema/Contact.Phone';
import Title_FIELD from '@salesforce/schema/Contact.Title';
import ID_FIELD from '@salesforce/schema/Contact.Id';
*/

const COLS = [
{ label: 'First Name', fieldName: 'FirstName', editable: true ,sortable:true},
{ label: 'Last Name', fieldName: 'LastName', editable: true,sortable:true },
{ label: 'Title', fieldName: 'Title',  editable: true,sortable:true},
{ label: 'Phone', fieldName: 'Phone', type: 'Phone', editable: true },
{ label: 'Email', fieldName: 'Email', type: 'email', editable: true }, 
{ label: 'View', initialWidth: 75, type: 'button-icon', typeAttributes: {
iconName: 'action:preview',
title: 'Preview',
variant: 'border-filled',
alternativeText: 'View'
} }
];
export default class Datatable_LWC extends LightningElement {

@track error;
@track data;
@track columns = COLS;
@track draftValues = [];
@track sortBy;
@track sortDirection;
@track rowOffset = 0;
@track bShowModal = false;
@track record = {};
@track loadMoreData;
@wire(getContactList)
contact(result) {
if (result.data) {
    this.data = result.data;
    this.error = undefined;
} else if (result.error) {
    this.error = result.error;
    this.data = undefined;
}
}



getSelectedName(event) {
const selectedRows = event.detail.selectedRows;
// Display that fieldName of the selected rows
for (let i = 0; i < selectedRows.length; i++){
    alert("You selected: " + selectedRows[i].FirstName);
}
}
handleSave(event) {
const recordInputs =  event.detail.draftValues.slice().map(draft => {
    const fields = Object.assign({}, draft);
    return { fields };
});
//alert(recordInputs[0].fields.Id)
//alert(this.datapro.fields)
//let row =  Object.assign({}, this.data);
let row = [...this.data];         
let copyrow;
let copydata = [];
this.data = [];
row.forEach(function(r){
    copyrow = {...r};
    for(let j=0;j<recordInputs.length;j++){             
    if(r.Id === recordInputs[j].fields.Id){
        if(recordInputs[j].fields.FirstName||recordInputs[j].fields.LastName||
            recordInputs[j].fields.Title||recordInputs[j].fields.Phone||recordInputs[j].fields.Email)
        { 
            if(recordInputs[j].fields.FirstName !=null)
            copyrow.FirstName = recordInputs[j].fields.FirstName;
            if(recordInputs[j].fields.Title !=null)
            copyrow.Title = recordInputs[j].fields.Title;
            if(recordInputs[j].fields.Phone !=null)
            copyrow.Phone = recordInputs[j].fields.Phone;
            if(recordInputs[j].fields.LastName !=null)
            copyrow.LastName = recordInputs[j].fields.LastName;   
            if(recordInputs[j].fields.Email !=null)
            copyrow.Email = recordInputs[j].fields.Email;
        }
    }
    }
    copydata.push(copyrow); 
});
this.data = [...copydata];
this.draftValues = [];  

}
handleSortdata(event) {
// field name
this.sortBy = event.detail.fieldName;

// sort direction
this.sortDirection = event.detail.sortDirection;

// calling sortdata function to sort the data based on direction and selected field
this.sortData(event.detail.fieldName, event.detail.sortDirection);
}

sortData(fieldname, direction) {

// serialize the data before calling sort function
let parseData = JSON.parse(JSON.stringify(this.data));

// Return the value stored in the field
let keyValue = (a) => {
    return a[fieldname];
};

// cheking reverse direction 
let isReverse = direction === 'asc' ? 1: -1;

// sorting data 
parseData.sort((x, y) => {
    x = keyValue(x) ? keyValue(x) : ''; // handling null values
    y = keyValue(y) ? keyValue(y) : '';

    // sorting values based on direction
    return isReverse * ((x > y) - (y > x));
});

// set the sorted data to data table data
this.data = parseData;

}

// Row Action event to show the details of the record
handleRowAction(event) {
const row = event.detail.row;
this.record = row;
this.bShowModal = true; // display modal window
}

// to close modal window set 'bShowModal' tarck value as false
closeModal() {
this.bShowModal = false;
}

loadMoreData(event) {
    alert("LOAD MORE ROWS")
    //Display a spinner to signal that data is being loaded
    event.target.isLoading = true;
    //Display "Loading" when more data is being loaded
    this.loadMoreStatus = 'Loading';
    const currentRecord = this.data;
    //const lastRecId = currentRecord[currentRecord.length - 1].Id;
    getContactList().then(result => {
        const currentData = result.sobList;
        //Appends new data to the end of the table
        const newData = currentRecord.concat(currentData);
        this.data = newData; 
        if (this.data.length >= this.totalNumberOfRows) {
            this.loadMoreStatus = 'No more data to load';
        } else {
            this.loadMoreStatus = '';
        }
    })
    .catch(error => {
        console.log('-------error-------------'+error);
        console.log(error);
    });
}

}