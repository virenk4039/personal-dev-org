import { LightningElement, track, api } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import saveNotes from "@salesforce/apex/CreateNote.saveNotes";
import getContents from "@salesforce/apex/CreateNote.getContents";
export default class Tem_RichTextEditor extends LightningElement {
  @track defaultText = "Compose text...";
  @api recordId;
  noteTitle;
  noteContent;
  @track noteList;
  @api
  get notes() {
    return this.notes;
  }
  set notes(values) {
    //this.noteList = values.AttachedContentNotes;
    this.noteList = JSON.parse(JSON.stringify(values));

    //notes fix
    let noteIdList = [];
    this.noteList.forEach((note) => {
      noteIdList.push(note.Id);
    });
    console.log("************ noteList pre getContents ************");
    console.log(JSON.parse(JSON.stringify(this.noteList)));

    getContents({ noteId: noteIdList /*values*/ })
      .then((contents) => {
        console.log("getContents success");
        this.noteList.forEach((note) => {
          let noteId = note.Id;
          let noteContent = contents[noteId];
          note.Content = noteContent;
          let date = new Date(note.LastModifiedDate);
          note.LastModifiedDate = date.toUTCString();
          note.LastModifiedBy = note.LastModifiedBy.Name;
        });
        console.log("************ noteList post getContents ************");
        console.log(JSON.parse(JSON.stringify(this.noteList)));
      })
      .catch((error) => {
        console.log("error on retrieving note contents");
        console.log(JSON.stringify(error));
      });
    if (this.noteList === undefined) {
      this.noteList = [];
    }
  }

  handleChange(event) {
    this.noteTitle = "First Note";
    this.noteContent = event.target.value;
  }

  handleSave(event) {
    event.preventDefault();
    saveNotes({
      noteTitle: this.noteTitle,
      noteContent: this.noteContent,
      oppRecord: this.recordId
    })
      .then((results) => {
        let tempArray = JSON.parse(JSON.stringify(this.noteList));
        let lastModifiedDate = results.contentNote.LastModifiedDate;
        let formattedDate = new Date(lastModifiedDate);
        formattedDate = formattedDate.toUTCString();
        let noteOwner = results.contentNote.LastModifiedBy.Name;
        let displayNote = {
          Title: this.noteTitle,
          Content: this.noteContent,
          LastModifiedDate: formattedDate,
          LastModifiedBy: noteOwner
        };
        // v.1.1 note location update, add note to top of array
        tempArray.unshift(displayNote);
        this.noteList = tempArray;
        const evt = new ShowToastEvent({
          title: "Note Created",
          variant: "success"
        });
        this.dispatchEvent(evt);
      })
      .catch((error) => {
        console.log("ERROR SAVING NOTE: " + JSON.stringify(error));
      });
  }
}