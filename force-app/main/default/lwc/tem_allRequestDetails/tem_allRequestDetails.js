import { LightningElement, wire, track } from "lwc";
import getRequestsByOpportunity from "@salesforce/apex/Checkoutreq.getRequestsByOpportunity";
import getLatestPlanProjectByOpportunity from "@salesforce/apex/BorrowedProject.getLatestPlanProjectByOpportunity";
import getQuote from "@salesforce/apex/AccountSelector.getQuote";
export default class Tem_allRequestDetails extends LightningElement {
  //opptyId = [];
  opptyId = new Set();
  // quoteId = [];
  quoteId = new Set();
  error;
  @track listToDisplay = [];
  records = [];
  @track columns = [
    { label: "Id", fieldName: "Id", type: "text", sortable: true },
    { label: "Type", fieldName: "Type__c", type: "text", sortable: true },
    { label: "Name", fieldName: "Name", type: "text", sortable: true },
    { label: "Quote Id", fieldName: "quoutId", type: "text", sortable: true },
    {
      label: "Opportunity",
      fieldName: "Opportunity__c",
      type: "text",
      sortable: true
    }
  ];

  @wire(getRequestsByOpportunity)
  requests({ data, error }) {
    if (data) {
      this.opportunityByRequestMap = new Map();
      data.forEach((eachRequest) => {
        if (this.opportunityByRequestMap.has(eachRequest.Opportunity__c)) {
          for (let key of this.opportunityByRequestMap
            .get(eachRequest.Opportunity__c)
            .keys()) {
            if (eachRequest.Type__c === key) {
              let subtype = this.opportunityByRequestMap
                .get(eachRequest.Opportunity__c)
                .get(eachRequest.Type__c);
              this.opportunityByRequestMap
                .get(eachRequest.Opportunity__c)
                .set(
                  eachRequest.Type__c,
                  subtype + ";" + eachRequest.Checked_out_By_Phone_Number__c
                );
            } else {
              this.opportunityByRequestMap
                .get(eachRequest.Opportunity__c)
                .set(
                  eachRequest.Type__c,
                  eachRequest.Checked_out_By_Phone_Number__c
                );
            }
          }
        } else {
          this.opportunityByRequestMap.set(
            eachRequest.Opportunity__c,
            new Map().set(
              eachRequest.Type__c,
              eachRequest.Checked_out_By_Phone_Number__c
            )
          );
        }

        let listToShow = {};
        if (
          eachRequest.Opportunity__c !== undefined &&
          eachRequest.Opportunity__r.AccountId
        ) {
          this.opptyId.add(eachRequest.Opportunity__c);
          this.quoteId.add(eachRequest.Opportunity__r.AccountId);
        }
        listToShow.Id = eachRequest.Id;
        listToShow.Type__c = eachRequest.Type__c;
        listToShow.Opportunity__c = eachRequest.Opportunity__c;
        listToShow.Account__c = eachRequest.Opportunity__r.AccountId;

        this.records.push(listToShow);
      });
      //  this.listToDisplay = this.records;
      this.opptyId = Array.from(this.opptyId);
      this.quoteId = Array.from(this.quoteId);

      this.opptyId = JSON.stringify(this.opptyId);
      this.quoteId = JSON.stringify(this.quoteId);
      this.fetchData();
      this.fetchQuoteData();
    } else if (error) {
      console.log(error);
    }
  }
  fetchData() {
    getLatestPlanProjectByOpportunity({ opportunityId: this.opptyId }).then(
      (data) => {
        if (data) {
          data.forEach((eachRequest) => {
            this.records.forEach((eachDisplayRecord) => {
              if (
                eachRequest.Opportunity__c === eachDisplayRecord.Opportunity__c
              ) {
                eachDisplayRecord.Name = eachRequest.Name;
              }
            });
          });
          //  this.listToDisplay = this.records;
        } else if (this.error) {
          console.log(this.error);
        }
      }
    );
  }

  fetchQuoteData() {
    getQuote({ quoteId: this.quoteId }).then((data) => {
      if (data) {
        data.forEach((eachQuoteRequest) => {
          this.records.forEach((eachDisplayRecord) => {
            if (
              eachQuoteRequest.Opportunity__c ===
              eachDisplayRecord.Opportunity__c
            ) {
              eachDisplayRecord.quoutId = eachQuoteRequest.Id;
            }
          });
        });
        this.listToDisplay = this.records;
      } else if (this.error) {
        console.log(this.error);
      }
    });
  }
}