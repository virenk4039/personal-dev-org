import { LightningElement, api, track } from "lwc";

export default class Org_retriveAccountData extends LightningElement {
  @api tabConfigs;
  @track finalTabData = [];
  connectedCallback() {
    if (this.tabConfigs) {
      let i = 0;
      let tempArray = [];
      let obj = {};
      this.tabConfigs.forEach((eachtabConfig) => {
        if (i === 2) {
          obj = {};
          obj.key = Math.random();
          obj.tempArray = tempArray;
          this.finalTabData.push(obj);
          i = 0;
          tempArray = [];
        }
        tempArray.push(eachtabConfig);
        i++;
      });

      if (tempArray && tempArray.length > 0) {
        obj = {};
        obj.key = Math.random();
        obj.tempArray = tempArray;
        this.finalTabData.push(obj);
      }
    }
  }
}