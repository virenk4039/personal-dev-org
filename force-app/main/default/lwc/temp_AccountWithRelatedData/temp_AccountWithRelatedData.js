/* eslint-disable no-unused-expressions */
/* eslint-disable @lwc/lwc/no-async-operation */
/* eslint-disable guard-for-in */
//eslint guard-for-in
import { LightningElement, api, track } from "lwc";
export default class Temp_AccountWithRelatedData extends LightningElement(
  LightningElement
) {
  @api recordId;

  @track FinalDisplayData = [
    {
      label: "DC",
      data: [
        {
          fieldValue: "Warm",
          fieldLabel: "Rating",
          isString: true
        },
        {
          fieldValue: "Burlington Textiles Corp of America",
          fieldLabel: "Account Name",
          isString: true
        },
        {
          fieldValue: this.number,
          fieldLabel: "Account Number",
          isNumber: true
        },
        {
          fieldValue: this.number,
          fieldLabel: "Account Number",
          isNumber: true
        }
      ]
    },
    {
      label: "DF",
      data: [
        {
          fieldValue: "Warm",
          fieldLabel: "Rating",
          isString: true
        },
        {
          fieldValue: "Burlington Textiles Corp of America",
          fieldLabel: "Account Name",
          isString: true
        },
        {
          fieldValue: this.number,
          fieldLabel: "Account Number",
          isNumber: true
        },
        {
          fieldValue: this.number,
          fieldLabel: "Account Number",
          isNumber: true
        }
      ]
    },
    {
      label: "HWS",
      data: [
        {
          fieldValue: "Warm",
          fieldLabel: "Rating",
          isString: true
        },
        {
          fieldValue: "Burlington Textiles Corp of America",
          fieldLabel: "Account Name",
          isString: true
        },
        {
          fieldValue: this.number,
          fieldLabel: "Account Number",
          isNumber: true
        }
      ]
    }
  ];
}