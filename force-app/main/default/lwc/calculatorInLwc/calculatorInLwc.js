import { LightningElement, track } from "lwc";
export default class CalculatorInLwc extends LightningElement {
  @track firstNumber;
  @track secondNumber;
  @track resultValue;
  handleNumberOeChange(event) {
    this.firstNumber = parseInt(event.target.value);
  }
  handleNumberTwoChange(event) {
    this.secondNumber = parseInt(event.target.value);
  }
  addition() {
    this.resultValue = parseInt(this.firstNumber) + parseInt(this.secondNumber);
  }
  multification() {
    console.log("this.firstNumber : " + this.firstNumber);
    console.log("this.secondNumber : " + this.secondNumber);
    this.resultValue = Number(this.firstNumber) * Number(this.secondNumber);
    // console.log("result : " + this.resultValue);
  }
  subtraction() {
    this.resultValue = this.firstNumber - this.secondNumber;
  }
  division() {
    this.resultValue = this.firstNumber / this.secondNumber;
  }
}