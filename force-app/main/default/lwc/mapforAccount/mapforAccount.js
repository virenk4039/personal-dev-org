import { LightningElement,api,wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
const FIELDS = [
    'Account.BillingState',
    'Account.BillingPostalCode',
    'Account.BillingStreet',
    'Account.BillingCity',
];

//import Account_Object from '@salesforce/schema/Account';
/*import BillingState from '@salesforce/schema/Account.BillingState';
import BillingPostalCode from '@salesforce/schema/Account.BillingPostalCode';
import BillingStreet from '@salesforce/schema/Account.BillingStreet';
import BillingCity from '@salesforce/schema/Account.BillingCity';*/
export default class LightningExampleMapSingleMarker extends LightningElement {
    @api recordId;

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    Account;

    mapMarkers = [
        {
            location: {
               /* Street: this.Account.data.fields.BillingStreet.value,
                City: this.Account.data.fields.BillingCity.value,
                State: this.Account.data.fields.BillingState.value,
                Country: this.Account.data.fields.BillingState.value,
                PostalCode: this.Account.data.fields.BillingPostalCode.value
                  */  

                Street: this.Account.BillingStreet,
                City: this.Account.BillingCity,
                State: this.Account.BillingState,
                Country: this.Account.BillingState,
                PostalCode: this.Account.BillingPostalCode
            },

            title: this.Account.BillingStreet,
            description:
            this.Account.BillingCity+''+ this.Account.BillingStreet +','+this.Account.BillingState
            +''+this.Account.BillingPostalCode
        }
    ];
    zoomLevel=15;
}