({
    doInit : function(component, event, helper) {
        var action = component.get('c.getChildRecord');
        action.setParams({
            "recordId" : component.get("v.recordId")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if(state === 'SUCCESS') {
                component.set('v.recordDetails', actionResult.getReturnValue());
            }         
        });
        
        $A.enqueueAction(action);
    },
    
    handleClickButton : function(component, event, helper) {
        component.set("v.showForm", true);
    },
    
    handleClick : function(component, event, helper) {
        if(component.get('v.recordDetails') !== '') {
            var editRecordEvent = $A.get("e.force:editRecord");
            editRecordEvent.setParams({
                "recordId": component.get("v.recordDetails")
            });
            editRecordEvent.fire();
        } else {
            var createAcountContactEvent = $A.get("e.force:createRecord");
            createAcountContactEvent.setParams({
                "entityApiName": "Checked_out_By__c",
                "defaultFieldValues": {
                    'Name' : 'Name',
                    'Borrowed_By__c' : component.get("v.recordId")
                }
            });
            createAcountContactEvent.fire();
        }
    }
})