({
	doinit : function(cmp, event, helper) {        
        cmp.set('v.mapMarkers', [
            {
                location: {                  
        			Country: cmp.get('v.simplerecord.BillingState'),
        			PostalCode: cmp.get('v.simplerecord.BillingPostalCode'),        
                    Street: cmp.get('v.simplerecord.BillingStreet'),
                    City: cmp.get('v.simplerecord.BillingCity'),
                    State: cmp.get('v.simplerecord.BillingState')
                },
				  icon: 'standard:account',
                title: cmp.get('v.simplerecord.BillingStreet'),
                description: cmp.get('v.simplerecord.BillingStreet') +', '+ 
                                cmp.get('v.simplerecord.BillingCity') +', '+
                                cmp.get('v.simplerecord.BillingState')+', '+
                                cmp.get('v.simplerecord.BillingPostalCode') +' '+
                                cmp.get('v.simplerecord.BillingState') 
               
            }
        ]);
	 cmp.set('v.zoomLevel', 15); 
	}
})