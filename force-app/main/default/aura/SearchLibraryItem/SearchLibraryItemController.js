({
      doInit: function(component, event, helper) {
        // Fetch the account list from the Apex controller
        helper.getItemList(component);
      },
    viewPicklist : function(component, event, helper) {
          component.set("v.showcomponent",true);
	},
    Filterresults: function(component, event, helper) {

        //values of event to this component
        var selectfield = event.getParam("selectfield");
        var selectcondition = event.getParam("selectcondition");
        var Enteredvalue = event.getParam("Enteredvalue");

        var action = component.get('c.getItemsFiltered');
         action.setParams({
            "selectfield" : selectfield,
            "selectcondition" : selectcondition,
            "Enteredvalue" : Enteredvalue
        });
          // Set up the callback
         action.setCallback(this, function(actionResult) {
         component.set('v.Book__c', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    }
    
      })