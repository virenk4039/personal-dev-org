({
    
      getItemList: function(component) {
        var action = component.get('c.getItem');
      
        action.setCallback(this, function(actionResult) {
         component.set('v.Book__c', actionResult.getReturnValue());
        });
     
      }
    })