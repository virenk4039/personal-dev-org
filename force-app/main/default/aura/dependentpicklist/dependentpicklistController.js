({
	
    //who want to fire event has to register - who has information
    //who has to listen event has to handler 
    
    filter: function(component, event, helper) {

        var SearchKeyChange = component.getEvent("searchKeyChange");
        //param is used to set the parameters to apex controller
        SearchKeyChange.setParams({
            
            "selectfield" : component.find("field").get("v.value"),
            "selectcondition" : component.find("condition").get("v.value"),
            "Enteredvalue" : component.find("comments").get("v.value")
        }); 
        //fire the event
        SearchKeyChange.fire();
    }
})